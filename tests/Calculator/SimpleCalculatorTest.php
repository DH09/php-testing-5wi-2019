<?php

namespace App\Tests\Calculator;

use App\Calculator\SimpleCalculator;
use PHPUnit\Framework\TestCase;

class SimpleCalculatorTest extends TestCase
{
    public function testAdd()
    {
        $calculator = new SimpleCalculator();
        $result = $calculator->add(12, 15);

        $this->assertEquals(27, $result);
    }
}
